import { Query } from "./query";

export type CustomerTextSearchQueryParams = string;
export type CustomerTextSearchQueryResponse = {
  id: string;
  name: string;
  email?: string | undefined;
  phoneNumber: string;
}[];
export type CustomerTextSearchQuery = Query<
  CustomerTextSearchQueryParams,
  {
    id: string;
    name: string;
    email?: string | undefined;
    phoneNumber: string;
  }[]
>;
