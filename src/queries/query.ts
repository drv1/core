export type Query<ParamType, ResType> = (params: ParamType) => ResType;
