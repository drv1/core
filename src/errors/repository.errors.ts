import { CoreError } from "./core.errors";

export class RepositoryError extends CoreError {
  name = "RepositoryError";
}
