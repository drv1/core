import { CoreError } from "./core.errors";

export class ValidationError extends CoreError {
  name = "ValidationError";
}
