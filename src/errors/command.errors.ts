import { CoreError } from "./core.errors";

export class CommandError extends CoreError {
  name = "CommandError";
}
