import { CoreError } from "./core.errors";

export class ModelError extends CoreError {
  name = "ModelError";
}
