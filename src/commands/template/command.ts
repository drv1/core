import { BaseCommand } from "@commands/base-command";
import { TemplateCommandRequest, TemplateCommandResponse } from "./types";

class TemplateCommand
  implements BaseCommand<TemplateCommandRequest, TemplateCommandResponse> {
  execute(req: TemplateCommandRequest): TemplateCommandResponse {
    throw new Error("Method not implemented.");
  }
}

export { TemplateCommand };
