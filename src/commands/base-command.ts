export interface BaseCommand<T, E> {
  execute(req: T): E;
}
