import { CommandError } from "@/errors";

class UpdateCustomerInfoCommandRequestInvalidIDError extends CommandError {
  name = "UpdateCustomerInfoCommandRequestInvalidIDError";
}

class UpdateCustomerInfoCommandRequestEmptyFieldsError extends CommandError {
  name = "UpdateCustomerInfoCommandRequestInvalidIDError";
}

export {
  UpdateCustomerInfoCommandRequestInvalidIDError,
  UpdateCustomerInfoCommandRequestEmptyFieldsError,
};
