import { BaseCommand } from "@/commands/base-command";
import { CustomerRepository } from "@/data/customer/customer.repository";
import { CommandError } from "@/errors";
import {
  UpdateCustomerInfoCommandRequest,
  UpdateCustomerInfoCommandResponse,
} from "./types";
import { UpdateCustomerInfoCommandRequestValidator } from "./validator";

class UpdateCustomerInfoCommand
  implements
    BaseCommand<
      UpdateCustomerInfoCommandRequest,
      UpdateCustomerInfoCommandResponse
    > {
  constructor(private _customerRepository: CustomerRepository) {}

  async execute(
    req: UpdateCustomerInfoCommandRequest
  ): UpdateCustomerInfoCommandResponse {
    UpdateCustomerInfoCommandRequestValidator.validate(req);

    const payload = {
      email: req.fields.email?.toLowerCase(),
      name: req.fields.name,
    };

    if (req.id.id !== null && req.id.id !== undefined)
      await this._customerRepository.updateInfoById(req.id.id, payload);
    else if (req.id.phoneNumber)
      await this._customerRepository.updateInfoByPhoneNumber(
        req.id.phoneNumber,
        payload
      );
    else throw new CommandError();
  }
}

export { UpdateCustomerInfoCommand };
