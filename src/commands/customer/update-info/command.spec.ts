import faker from "faker";
import { CustomerRepository } from "@data/customer/customer.repository";
import { UpdateCustomerInfoCommand } from "./command";
import {
  CustomerJsonRepository,
  JsonStore,
} from "@/data/mock/customer.json-repository";
import { Customer } from "@/models/customer";
import {
  UpdateCustomerInfoCommandRequestEmptyFieldsError,
  UpdateCustomerInfoCommandRequestInvalidIDError,
} from "./errors";

const formGenerator = (): {
  name: string;
  email: string;
  phoneNumber: string;
} => {
  const firstName = faker.name.firstName();
  const lastName = faker.name.lastName();

  const name = `${firstName} ${lastName}`;
  const email = faker.internet.email(firstName, lastName);
  const phoneNumber = faker.phone.phoneNumber("+55 (##) ####-####");

  return { name, email, phoneNumber };
};

describe("Delete customer command", () => {
  let repo: CustomerRepository;
  let command: UpdateCustomerInfoCommand;
  let store = { data: {} as JsonStore };
  let data: {
    name: string;
    email: string;
    phoneNumber: string;
  };

  let customer: Customer;
  const getStore = () => store.data;
  const setStore = (v: JsonStore) => (store.data = v);
  const getUserInsideDb = () => getStore()[data.phoneNumber + ""];

  const seed = 0;
  faker.seed(seed);
  faker.locale = "pt_BR";

  beforeEach(async () => {
    repo = new CustomerJsonRepository({}, setStore);
    data = formGenerator();
    await repo.create(data);
    customer = await repo.findByPhoneNumber(data.phoneNumber);
    command = new UpdateCustomerInfoCommand(repo);
  });

  test("Update customer name", async () => {
    expect(getUserInsideDb().name).toStrictEqual(data.name);

    const newName = `${faker.name.firstName()} ${faker.name.lastName()}`;
    await command.execute({
      id: { phoneNumber: data.phoneNumber },
      fields: { name: newName },
    });

    expect(getUserInsideDb().name).toStrictEqual(newName);
  });

  test("Update customer email", async () => {
    expect(getUserInsideDb().email?.toLowerCase()).toStrictEqual(
      data.email.toLowerCase()
    );
    const newEmail = faker.internet.email();
    await command.execute({
      id: { phoneNumber: data.phoneNumber },
      fields: { email: newEmail },
    });

    expect(getUserInsideDb().email).toStrictEqual(newEmail.toLowerCase());
  });

  test("Update customer email using id", async () => {
    expect(getUserInsideDb().email?.toLowerCase()).toStrictEqual(
      data.email.toLowerCase()
    );
    const newEmail = faker.internet.email();
    await command.execute({
      id: { id: getUserInsideDb().id },
      fields: { email: newEmail },
    });

    expect(getUserInsideDb().email).toStrictEqual(newEmail.toLowerCase());
  });

  test("Prevent useless updates", async () => {
    const err = await command
      .execute({
        id: { id: getUserInsideDb().id },
        fields: {},
      })
      .catch((e) => e);

    expect(err).toBeInstanceOf(
      UpdateCustomerInfoCommandRequestEmptyFieldsError
    );
  });

  test("Throw error if request doesn't contain a valid ID", async () => {
    const email = faker.internet.email();
    const name = faker.name.findName();

    const err = await command
      .execute({
        id: {},
        fields: { email, name },
      })
      .catch((e) => e);

    expect(err).toBeInstanceOf(UpdateCustomerInfoCommandRequestInvalidIDError);
  });
});
