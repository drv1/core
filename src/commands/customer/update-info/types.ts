type UpdateCustomerInfoCommandRequest = {
  id: {
    id?: number;
    phoneNumber?: string;
  };
  fields: {
    name?: string;
    email?: string;
  };
};

type UpdateCustomerInfoCommandResponse = Promise<void>;

export { UpdateCustomerInfoCommandRequest, UpdateCustomerInfoCommandResponse };
