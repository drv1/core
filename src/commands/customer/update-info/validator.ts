import {
  UpdateCustomerInfoCommandRequestInvalidIDError,
  UpdateCustomerInfoCommandRequestEmptyFieldsError,
} from "./errors";
import { UpdateCustomerInfoCommandRequest } from "./types";

class UpdateCustomerInfoCommandRequestValidator {
  static validate(req: UpdateCustomerInfoCommandRequest) {
    if (
      !req.id ||
      (!req.id.phoneNumber && (req.id.id === undefined || req.id.id === null))
    )
      throw new UpdateCustomerInfoCommandRequestInvalidIDError();

    if (!req.fields || (!req.fields.email && !req.fields.name))
      throw new UpdateCustomerInfoCommandRequestEmptyFieldsError();
  }
}

export { UpdateCustomerInfoCommandRequestValidator };
