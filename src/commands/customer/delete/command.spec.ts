import faker from "faker";

import { CustomerRepository } from "@data/customer/customer.repository";
import {
  CustomerJsonRepository,
  JsonStore,
} from "@data/mock/customer.json-repository";
import { DeleteCustomerCommand } from "./command";
import { Customer } from "@/models/customer";
import { CustomerRepositoryPhoneNumberNotFoundError } from "@/data/customer/customer.repository.errors";

describe("Create customer command", () => {
  let repo: CustomerRepository;
  let command: DeleteCustomerCommand;
  let store = { data: {} as JsonStore };
  let data: {
    name: string;
    email: string;
    phoneNumber: string;
  };
  const getStore = () => store.data;
  const setStore = (v: JsonStore) => (store.data = v);

  const seed = 0;
  faker.seed(seed);
  faker.locale = "pt_BR";

  const formGenerator = (): {
    name: string;
    email: string;
    phoneNumber: string;
  } => {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();

    const name = `${firstName} ${lastName}`;
    const email = faker.internet.email(firstName, lastName);
    const phoneNumber = faker.phone.phoneNumber("+55 (##) ####-####");

    return { name, email, phoneNumber };
  };

  beforeEach(async () => {
    repo = new CustomerJsonRepository(getStore(), setStore);
    data = formGenerator();
    await repo.create(data);
    command = new DeleteCustomerCommand(repo);
  });

  test("Delete customer", async () => {
    expect(getStore()[data.phoneNumber + ""]).toBeTruthy();
    expect(() =>
      command.execute({ phoneNumber: data.phoneNumber })
    ).not.toThrow();
    expect(getStore()[data.phoneNumber + ""]).toBeFalsy();
  });

  test("Throw error if customer is not in database", async () => {
    const req = formGenerator();
    req.phoneNumber = "11 912345678";
    const err = await command.execute(req).catch((e) => e);
    expect(err).toBeInstanceOf(CustomerRepositoryPhoneNumberNotFoundError);
  });
});
