import { BaseCommand } from "@/commands/base-command";
import { CustomerRepository } from "@/data/customer/customer.repository";
import {
  DeleteCustomerCommandRequest,
  DeleteCustomerCommandResponse,
} from "./types";

class DeleteCustomerCommand
  implements
    BaseCommand<DeleteCustomerCommandRequest, DeleteCustomerCommandResponse> {
  constructor(private _customerRepository: CustomerRepository) {}

  async execute(
    req: DeleteCustomerCommandRequest
  ): DeleteCustomerCommandResponse {
    await this._customerRepository.deleteByPhoneNumber(req.phoneNumber);
  }
}

export { DeleteCustomerCommand };
