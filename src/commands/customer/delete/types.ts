type DeleteCustomerCommandRequest = {
  phoneNumber: string;
};

type DeleteCustomerCommandResponse = Promise<void>;

export { DeleteCustomerCommandRequest, DeleteCustomerCommandResponse };
