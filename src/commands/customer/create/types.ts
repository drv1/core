export interface CreateCustomerCommandRequest {
  name: string;
  phoneNumber: string;
  email?: string;
}
