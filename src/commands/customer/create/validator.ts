import validator from "validator";
import { CreateCustomerCommandRequest } from "./types";

abstract class CreateCustomerCommandRequestValidator {
  validate(req: CreateCustomerCommandRequest) {
    const fields = [];

    try {
      req.name.toString();
    } catch {
      fields.push("name");
    }
  }
}

export {};
