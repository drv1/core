import faker from "faker";

import { CustomerRepository } from "@data/customer/customer.repository";
import {
  CustomerJsonRepository,
  JsonStore,
} from "@data/mock/customer.json-repository";
import { CreateCustomerCommand } from "./command";
import { CreateCustomerCommandRequest } from "./types";
import { CustomerValidationError } from "@/models/customer/errors";

describe("Create customer command", () => {
  let repo: CustomerRepository;
  let command: CreateCustomerCommand;
  let store = { data: {} as JsonStore };
  const getStore = () => store.data;
  const setStore = (v: JsonStore) => (store.data = v);

  const seed = 0;
  faker.seed(seed);
  faker.locale = "pt_BR";

  const formGenerator = (): CreateCustomerCommandRequest => {
    const firstName = faker.name.firstName();
    const lastName = faker.name.lastName();

    const name = `${firstName} ${lastName}`;
    const email = faker.internet.email(firstName, lastName);
    const phoneNumber = faker.phone.phoneNumber("+55 (##) ####-####");

    return { name, email, phoneNumber };
  };

  beforeEach(() => {
    setStore({});
    repo = new CustomerJsonRepository({}, setStore);
    command = new CreateCustomerCommand(repo);
  });

  test("Insert customer correctly", async () => {
    const req = formGenerator();
    expect(() => command.execute(req)).not.toThrow();
    expect(getStore()[req.phoneNumber + ""]).toBeTruthy();
  });

  test("Prevent customer insertion w/ invalid phone", async () => {
    const req = formGenerator();
    req.phoneNumber = "11 912345678";
    const err = await command.execute(req).catch((e) => e);
    expect(err).toBeInstanceOf(CustomerValidationError);
    if (err instanceof CustomerValidationError) {
      expect(err.invalidFields).toContain("phoneNumber");
    }
  });
});
