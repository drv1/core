import { CreateCustomerCommandRequest } from "./types";
import { BaseCommand } from "@commands/base-command";
import { CustomerRepository } from "@data/customer/customer.repository";
import { CustomerValidator } from "@/models/customer/validator";

export class CreateCustomerCommand
  implements BaseCommand<CreateCustomerCommandRequest, Promise<void>> {
  constructor(private customerRepository: CustomerRepository) {}

  async execute(req: CreateCustomerCommandRequest): Promise<void> {
    CustomerValidator.validate(req);
    await this.customerRepository.create(req);
  }
}
