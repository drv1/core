import { Customer } from "@models/customer";
import { CustomerRepository } from "../customer/customer.repository";
import {
  CustomerRepositoryDuplicatePhoneNumberError,
  CustomerRepositoryIdNotFoundError,
  CustomerRepositoryPhoneNumberNotFoundError,
} from "../customer/customer.repository.errors";

interface SerializedCustomer {
  id: number;
  name: string;
  email?: string;
  phoneNumber: string;
}

export type JsonStore = {
  [phoneNumber: string]: SerializedCustomer;
};

export class CustomerJsonRepository implements CustomerRepository {
  constructor(
    private store: JsonStore = {},
    private mutationCallback?: (store: JsonStore) => any
  ) {}

  async create(customer: Omit<Customer, "id">): Promise<void> {
    const key = customer.phoneNumber;
    if (this.store[key])
      throw new CustomerRepositoryDuplicatePhoneNumberError();

    const { name, email, phoneNumber } = customer;
    const id = Object.values(this.store).length;

    this.store[key] = { id, name, email, phoneNumber };
    this.mutate();
  }

  async deleteByPhoneNumber(phoneNumber: string): Promise<void> {
    if (this.store[phoneNumber]) {
      delete this.store[phoneNumber];
      this.mutate();
    } else {
      throw new CustomerRepositoryPhoneNumberNotFoundError();
    }
  }

  async findById(id: number): Promise<Customer> {
    const candidates = Object.values(this.store).filter((x) => x.id === id);
    if (candidates.length === 0) throw new CustomerRepositoryIdNotFoundError();
    const serializedCustomer = candidates[0];

    return new Customer(
      serializedCustomer.name,
      serializedCustomer.phoneNumber,
      serializedCustomer.id,
      serializedCustomer.email
    );
  }

  async findByPhoneNumber(phoneNumber: string): Promise<Customer> {
    if (this.store[phoneNumber]) {
      const data = this.store[phoneNumber];
      return new Customer(data.name, data.phoneNumber, data.id, data.email);
    } else {
      throw new CustomerRepositoryPhoneNumberNotFoundError();
    }
  }

  async updateInfoById(
    id: number,
    data: { name?: string; email?: string }
  ): Promise<void> {
    const candidates = Object.values(this.store).filter((x) => x.id === id);
    if (candidates.length === 0) throw new CustomerRepositoryIdNotFoundError();
    const serializedCustomer = candidates[0];
    serializedCustomer.email = data.email ?? serializedCustomer.email;
    serializedCustomer.name = data.name ?? serializedCustomer.name;

    this.store[serializedCustomer.phoneNumber] = serializedCustomer;
    this.mutate();
  }

  async updateInfoByPhoneNumber(
    phoneNumber: string,
    data: { name?: string; email?: string }
  ): Promise<void> {
    if (!this.store[phoneNumber])
      throw new CustomerRepositoryPhoneNumberNotFoundError();
    const serializedCustomer = this.store[phoneNumber];

    serializedCustomer.email = data.email ?? serializedCustomer.email;
    serializedCustomer.name = data.name ?? serializedCustomer.name;

    this.store[serializedCustomer.phoneNumber] = serializedCustomer;
    this.mutate();
  }

  private mutate() {
    if (this.mutationCallback) this.mutationCallback(this.store);
  }
}
