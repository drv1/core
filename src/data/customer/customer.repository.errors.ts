import { RepositoryError } from "@errors";

export class CustomerRepositoryDuplicatePhoneNumberError extends RepositoryError {
  name = "CustomerRepositoryDuplicatePhoneNumberError";
}

export class CustomerRepositoryPhoneNumberNotFoundError extends RepositoryError {
  name = "CustomerRepositoryPhoneNumberNotFoundError";
}

export class CustomerRepositoryIdNotFoundError extends RepositoryError {
  name = "CustomerRepositoryIdNotFoundError";
}
