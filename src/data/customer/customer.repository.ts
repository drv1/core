import { Customer } from "@models/customer";

export interface CustomerRepository {
  create(customerData: {
    name: string;
    phoneNumber: string;
    email?: string;
  }): Promise<void>;
  deleteByPhoneNumber(phoneNumber: string): Promise<void>;
  findById(id: number): Promise<Customer>;
  findByPhoneNumber(phoneNumber: string): Promise<Customer>;
  updateInfoById(
    id: number,
    data: { name?: string; email?: string }
  ): Promise<void>;
  updateInfoByPhoneNumber(
    phoneNumber: string,
    data: { name?: string; email?: string }
  ): Promise<void>;
}
