export type TCustomer = {
  id: number;
  name: string;
  email?: string;
  phoneNumber: string;
};
