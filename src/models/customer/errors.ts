import { ValidationError } from "@errors";
import { Customer } from "./customer";

export class CustomerValidationError extends ValidationError {
  name = "CustomerValidationError";

  constructor(
    public invalidFields: string[],
    public entity?: Partial<Customer>
  ) {
    super();
  }
}
