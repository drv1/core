import validator from "validator";
import { TCustomer } from "../types/t-customer";
import { CustomerValidationError } from "./errors";

export abstract class CustomerValidator {
  static validateName(s: string) {
    return [
      // (s: string) => validator.isLength(s, { min: 1, max: 256 }),
      (s: string) =>
        /^[A-Za-zÀ-ÿ\u00f1\u00d1]+(?: +[A-Za-zÀ-ÿ\u00f1\u00d1]+)*$/.test(s),
    ].every((x) => x(s));
  }

  static validateEmail(s: string) {
    return [(s: string) => validator.isEmail(s)].every((x) => x(s));
  }

  static validatePhoneNumber(s: string) {
    return [
      (s: string) =>
        validator.isMobilePhone(s, ["pt-BR"], { strictMode: true }),
    ].every((x) => x(s));
  }

  static validate(customer: Omit<TCustomer, "id">) {
    const invalidFields: string[] = [];

    if (!this.validateName(customer.name)) invalidFields.push("name");

    if (customer.email && !this.validateEmail(customer.email))
      invalidFields.push("email");

    if (!this.validatePhoneNumber(customer.phoneNumber))
      invalidFields.push("phoneNumber");

    if (invalidFields.length > 0) {
      throw new CustomerValidationError(invalidFields, customer);
    }
  }
}
