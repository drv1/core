import { TCustomer } from "../types/t-customer";

export class Customer implements TCustomer {
  id: number;
  name: string;
  email?: string;
  phoneNumber: string;

  constructor(name: string, phoneNumber: string, id: number, email?: string) {
    this.name = name;
    this.email = email?.toLowerCase();
    this.phoneNumber = phoneNumber;
    this.id = id;
  }
}
